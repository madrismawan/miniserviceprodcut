package com.maderismawan.product.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maderismawan.product.entities.Inventory;
import com.maderismawan.product.entities.Product;
import com.maderismawan.product.repositories.InventoryRepo;

import jakarta.persistence.EntityNotFoundException;

import java.util.Comparator;
import java.util.List;

@Service
public class InventoryService {
    
    @Autowired
    private final InventoryRepo inventoryRepo;

    public InventoryService(InventoryRepo inventoryRepo){
        this.inventoryRepo = inventoryRepo;
    }

    public List<Inventory> findAll() {
        return inventoryRepo.findAll().stream().sorted(Comparator.comparing(Inventory::getId)).toList();
    }

    public List<Inventory> finAllByProduct(Product product){
        return inventoryRepo.findByProduct(product);
    }

    public Inventory create(Inventory inventory){
        return inventoryRepo.save(inventory);
    }

    public Inventory findById(Long id) {
        return inventoryRepo.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Transactional
    public Inventory updateStockAmount(Long id, Integer trxQty){
        Inventory inventory = findById(id);
        Integer currStock = inventory.getStockAmount() - trxQty;
        if(currStock == 0){
            inventory.setIsActive(false);
        }
        inventory.setStockAmount(currStock);
        return inventoryRepo.save(inventory);
    }
}
