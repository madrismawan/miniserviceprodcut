package com.maderismawan.product.services;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maderismawan.product.dto.stocks.StockReport;
import com.maderismawan.product.entities.Inventory;
import com.maderismawan.product.entities.Product;

@Service
public class ReportService {
    @Autowired
    private final ProductService productService;

    @Autowired
    private final InventoryService inventoryService;

    public ReportService(ProductService productService, InventoryService inventoryService){
        this.productService = productService;
        this.inventoryService = inventoryService;
    }

    
    public Map<Long, Integer> filterTheStock(List<Inventory> listInventory){
        return listInventory.stream()
            .filter(inventory -> Boolean.TRUE.equals(inventory.getIsActive()))
            .collect(Collectors.groupingBy(
                inventory -> inventory.getProduct().getId(),
                Collectors.summingInt(Inventory::getStockAmount)
            ));
    } 

    public List<StockReport> reportStock(){
        List<Inventory> listInventory = inventoryService.findAll();

        Map<Long, Integer> productStockAmountMap = filterTheStock(listInventory);

        return productStockAmountMap.entrySet().stream()
            .map(entry -> new StockReport(entry.getKey(), entry.getValue()))
            .collect(Collectors.toList());
    }


    public StockReport checkStockByProduct(Long product_id){
        Product product = productService.findById(product_id);
        if(product == null){    
            return new StockReport(product_id, null);
        }
        List<Inventory> listInventoryProduct = inventoryService.finAllByProduct(product);

        Map<Long, Integer> productStockAmountMap = filterTheStock(listInventoryProduct);

        Map.Entry<Long, Integer> entry = productStockAmountMap.entrySet().iterator().next();
        return new StockReport(entry.getKey(), entry.getValue());
    }
}
