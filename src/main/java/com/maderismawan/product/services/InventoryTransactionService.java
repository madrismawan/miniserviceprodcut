package com.maderismawan.product.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maderismawan.product.entities.InventoryTransaction;
import com.maderismawan.product.repositories.InventoryTransactionRepo;

@Service
public class InventoryTransactionService {
    @Autowired
    private final InventoryTransactionRepo inventoryTransactionRepo;

    public InventoryTransactionService(InventoryTransactionRepo inventoryTransactionRepo){
        this.inventoryTransactionRepo = inventoryTransactionRepo;
    }

     public InventoryTransaction create(InventoryTransaction inventoryTransaction){
        return inventoryTransactionRepo.save(inventoryTransaction);
    }

}
