package com.maderismawan.product.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maderismawan.product.dto.products.CreateProductDTO;
import com.maderismawan.product.entities.Product;
import com.maderismawan.product.repositories.ProductRepo;
import com.maderismawan.product.services.interfaces.ProductServiceInterface;

import jakarta.persistence.EntityNotFoundException;

@Service
public class ProductService implements ProductServiceInterface{
    
    @Autowired
    private final ProductRepo repository;
    
    public ProductService(ProductRepo productRepo){
        this.repository = productRepo;
    }

    public Product create(CreateProductDTO input) {
        Product product = input.toProduct();
        return repository.save(product);
    }

    public Product findById(Long id) {
        return repository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    public Boolean exixtsProductById(Long id) {
        return repository.existsById(id);
    }

}
