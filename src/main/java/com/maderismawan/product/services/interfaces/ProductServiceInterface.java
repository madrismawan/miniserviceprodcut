package com.maderismawan.product.services.interfaces;
import com.maderismawan.product.dto.products.CreateProductDTO;
import com.maderismawan.product.entities.Product;

public interface ProductServiceInterface {
  Product create(CreateProductDTO input);

  Product findById(Long id);

  Boolean exixtsProductById(Long id);  
  
}