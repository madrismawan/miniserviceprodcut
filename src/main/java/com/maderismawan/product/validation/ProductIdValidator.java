package com.maderismawan.product.validation;

import com.maderismawan.product.services.ProductService;
import com.maderismawan.product.validation.annotation.ExitsProductId;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class ProductIdValidator implements ConstraintValidator<ExitsProductId, Long>{
    private final ProductService productService;

    public ProductIdValidator(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public boolean isValid(Long productId, ConstraintValidatorContext context) {
        // Check if the product_id exists in the database
        return productId != null && productService.exixtsProductById(productId);
    }
}
