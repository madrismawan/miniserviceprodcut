package com.maderismawan.product.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.maderismawan.product.dto.stocks.StockReport;
import com.maderismawan.product.services.ReportService;
import com.maderismawan.product.utilities.ApiResponse;

import java.util.List;


@RestController
@RequestMapping(value = "/report")
public class ReportController {

    private final ReportService reportService;
    
    public ReportController(ReportService reportService){
        this.reportService = reportService;
    }
    
    @GetMapping("/stock")
    public ResponseEntity<ApiResponse<List<StockReport>>> getReportStock() {
        List<StockReport> reports = reportService.reportStock();
        return ApiResponse.handleResponse(HttpStatus.OK, reports);
    }

    @GetMapping("/stock/product/{id}")
    public ResponseEntity<ApiResponse<StockReport>> getReportStockByProduct(@PathVariable("id") Long id) {
        StockReport reports = reportService.checkStockByProduct(id);
        return ApiResponse.handleResponse(HttpStatus.OK, reports);
    }
}
