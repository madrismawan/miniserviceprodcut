package com.maderismawan.product.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.maderismawan.product.dto.inventories.CreateInventoryDTO;
import com.maderismawan.product.entities.Inventory;
import com.maderismawan.product.managers.InventoryManager;
import com.maderismawan.product.services.InventoryService;
import com.maderismawan.product.utilities.ApiResponse;

import jakarta.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/inventory")
public class InventoryController {

    private final InventoryManager inventoryManager;

    private final InventoryService inventoryService;

    public InventoryController(InventoryManager inventoryManager, InventoryService inventoryService){
        this.inventoryManager = inventoryManager;
        this.inventoryService = inventoryService;
    }
 
    @GetMapping
    public ResponseEntity<ApiResponse<List<Inventory>>> getAllInventories() {
        List<Inventory> inventories = inventoryService.findAll();
        return ApiResponse.handleResponse(HttpStatus.OK, inventories);
    }

    @PostMapping()
    public ResponseEntity<ApiResponse<Inventory>> createInventory(@Valid @RequestBody CreateInventoryDTO req) {
        Inventory inventory = inventoryManager.create(req);
        return ApiResponse.handleResponse(HttpStatus.CREATED, inventory);
    }

    
    @GetMapping("/product/{id}")
    public ResponseEntity<ApiResponse<List<Inventory>>> getAllByProduct(@PathVariable("id") Long id) {
        List<Inventory> inventories = inventoryManager.findAllByProduct(id);
        return ApiResponse.handleResponse(HttpStatus.OK, inventories);
    }

}