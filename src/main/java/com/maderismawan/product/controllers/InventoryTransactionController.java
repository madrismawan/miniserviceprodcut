package com.maderismawan.product.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.maderismawan.product.dto.inventoryTransaction.CreateOutboundDTO;
import com.maderismawan.product.dto.inventoryTransaction.ResponseCreateOutboundDTO;
import com.maderismawan.product.managers.InventoryTransactionManager;
import com.maderismawan.product.utilities.ApiResponse;

import jakarta.validation.Valid;

@RestController
@RequestMapping(value = "/inventory-transaction")
public class InventoryTransactionController {
    
    private final InventoryTransactionManager inventoryTransactionManager;

    public InventoryTransactionController(InventoryTransactionManager inventoryTransactionManager){
        this.inventoryTransactionManager = inventoryTransactionManager;
    }

    @PostMapping("/outbound")
    public ResponseEntity<ApiResponse<ResponseCreateOutboundDTO>> createOutbound(@Valid @RequestBody CreateOutboundDTO req){
        ResponseCreateOutboundDTO response = inventoryTransactionManager.createOutbound(req);
        return ApiResponse.handleResponse(response.getHttpStatus(),response.getMessage());
    }
}
