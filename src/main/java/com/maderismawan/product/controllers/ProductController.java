package com.maderismawan.product.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.maderismawan.product.dto.products.CreateProductDTO;
import com.maderismawan.product.entities.Product;
import com.maderismawan.product.services.ProductService;
import com.maderismawan.product.utilities.ApiResponse;

import jakarta.validation.Valid;

@RestController
@RequestMapping(value = "/product")
public class ProductController {
    
    private final ProductService productService;

    public ProductController(ProductService serivce){
        this.productService = serivce;
    }

    @PostMapping()
    public ResponseEntity<ApiResponse<Product>> createProduct(@Valid @RequestBody CreateProductDTO req) {
        Product product  = productService.create(req);
        return ApiResponse.handleResponse(HttpStatus.CREATED, product);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse<Product>> findProductById(@PathVariable("id") Long id) {
        Product product = productService.findById(id);
        return ApiResponse.handleResponse(HttpStatus.OK, product);
    }


}
