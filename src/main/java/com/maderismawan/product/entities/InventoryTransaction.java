package com.maderismawan.product.entities;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maderismawan.product.enums.RefTypeTransaction;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Table(name = "inventory_transaction")
public class InventoryTransaction extends BaseEntity{
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "inventory_id", nullable = false)
    private Inventory inventory;

    @Column(name = "ref_id")
    private String RefId;

    @Column(name = "prev_amount")
    private Integer prevAmount;

    @Column(name = "trx_amount")
    private Integer trxAmount;

    @Column(name = "curr_amount")
    private Integer currAmount;

    @Enumerated(EnumType.STRING)
    @Column(name = "ref_type")
    private RefTypeTransaction refType;

}
