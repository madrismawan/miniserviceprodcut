package com.maderismawan.product.utilities;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiResponse<T> {
    private String message;
    private T data;

    public ApiResponse(String message, T data) {
        this.message = message;
        this.data = data;
    }

     public static <T> ResponseEntity<ApiResponse<T>> handleResponse(HttpStatus statusCode, T data) {
        return ResponseEntity.status(statusCode).body(new ApiResponse<T>(statusCode.getReasonPhrase(), data));
    }

    public static <T> ResponseEntity<ApiResponse<T>> handleResponse(HttpStatus statusCode) {
        return ResponseEntity.status(statusCode).body(new ApiResponse<T>(statusCode.getReasonPhrase(), null));
    }

    public static <T> ResponseEntity<ApiResponse<T>> handleResponse(HttpStatus statusCode, String message) {
        return ResponseEntity.status(statusCode).body(new ApiResponse<T>(message, null));
    }

}
