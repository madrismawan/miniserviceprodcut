package com.maderismawan.product.sdk.transaction.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.maderismawan.product.dto.inventories.CreateBatchInventoryDTO;
import com.maderismawan.product.sdk.transaction.dto.ResponseAPI;
import com.maderismawan.product.sdk.transaction.services.InboundService;

import jakarta.validation.Valid;

import org.springframework.http.ResponseEntity;


@RestController
@RequestMapping(value = "/open")
public class InboundController {

    private final InboundService inboundService;

    public InboundController(InboundService inboundService){
        this.inboundService = inboundService; 
    }

    @PostMapping("/inbound")
    public ResponseEntity<String> createInbound(@Valid @RequestBody CreateBatchInventoryDTO req){
        ResponseAPI response = inboundService.createBatch(req); 
        return ResponseEntity.status(response.getHttpStatus()).body(response.getMessage());
    }


}
