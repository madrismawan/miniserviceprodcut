package com.maderismawan.product.sdk.transaction.dto;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseAPI {
    private String message;
    private HttpStatus httpStatus;

    public ResponseAPI(String message,HttpStatus httpStatus){
        this.message = message;
        this.httpStatus = httpStatus;
    }
}
