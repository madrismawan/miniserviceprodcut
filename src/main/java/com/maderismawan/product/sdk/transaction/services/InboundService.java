package com.maderismawan.product.sdk.transaction.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.maderismawan.product.dto.inventories.CreateBatchInventoryDTO;
import com.maderismawan.product.managers.InventoryManager;
import com.maderismawan.product.sdk.transaction.dto.ResponseAPI;

@Service
public class InboundService {
    @Autowired
    private final InventoryManager inventoryManager;

    public InboundService(InventoryManager inventoryManager){
        this.inventoryManager = inventoryManager;
    }

    public ResponseAPI createBatch(CreateBatchInventoryDTO req){
        inventoryManager.createBatch(req);
        return new ResponseAPI( "Success create inbound", HttpStatus.CREATED);
    }

}
