package com.maderismawan.product.dto.products;

import org.springframework.stereotype.Component;

import com.maderismawan.product.entities.Product;

import io.micrometer.common.lang.Nullable;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class CreateProductDTO {
    
    @NotNull
    @Size(min = 4, max = 16, message = "The length of barcode must be between 4 and 16 characters.")
    private String barcode;
    
    @NotEmpty(message = "The full name is required.")
    private String name;
    
    @Nullable
    private String description;
    
    public Product toProduct(){
        return new Product()
            .setBarcode(barcode)
            .setName(name)            
            .setDescription(description)
            .setIsActive(true);
    }

}
