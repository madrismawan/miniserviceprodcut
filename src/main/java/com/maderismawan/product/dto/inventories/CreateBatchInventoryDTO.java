package com.maderismawan.product.dto.inventories;

import java.sql.Date;
import java.util.List;

import com.maderismawan.product.dto.Item;
import com.maderismawan.product.enums.RefTypeTransaction;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateBatchInventoryDTO {
    @NotNull
    private String refId;

    @NotNull
    private String location;

    @Enumerated(EnumType.STRING)
    @NotNull
    private RefTypeTransaction refType;

    @NotNull
    private Date expiredDate;

    @NotNull
    private List<Item> details;
}
