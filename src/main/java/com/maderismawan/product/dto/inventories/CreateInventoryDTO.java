package com.maderismawan.product.dto.inventories;
import java.sql.Date;

import org.springframework.stereotype.Component;

import com.maderismawan.product.entities.Inventory;
import com.maderismawan.product.enums.RefTypeTransaction;
import com.maderismawan.product.validation.annotation.ExitsProductId;

import io.micrometer.common.lang.Nullable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Component
public class CreateInventoryDTO {
    
    @Nullable
    private String RefId;

    @ExitsProductId
    private Long productId;

    @Min(1)
    private Integer qty;

    @NotNull
    private String location;

    @Enumerated(EnumType.STRING)
    @NotNull
    private RefTypeTransaction refType;
        
    @NotNull
    private Date expiredDate;

    public Inventory toInventory(){
        return new Inventory()
            .setRefId(RefId)
            .setStockAmount(qty)            
            .setLocation(location)
            .setExpiredData(expiredDate)
            .setIsActive(true);
    }
}
