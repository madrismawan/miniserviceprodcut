package com.maderismawan.product.dto;

import com.maderismawan.product.validation.annotation.ExitsProductId;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Item {
    @NotNull
    @ExitsProductId
    private Long productId;

    @NotNull
    private Integer qty;
}