package com.maderismawan.product.dto.stocks;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StockReport {

    private Long productId;

    private Integer total;

    public StockReport(Long productId, Integer total) {
        this.productId = productId;
        this.total = total;
    }
}
