package com.maderismawan.product.dto.inventoryTransaction;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseCreateOutboundDTO {
    private String message;
    private HttpStatus httpStatus;

    public ResponseCreateOutboundDTO(String message,HttpStatus httpStatus){
        this.message = message;
        this.httpStatus = httpStatus;
    }

}
