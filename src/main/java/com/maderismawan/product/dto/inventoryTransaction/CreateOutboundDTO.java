package com.maderismawan.product.dto.inventoryTransaction;

import org.springframework.stereotype.Component;

import com.maderismawan.product.enums.RefTypeTransaction;
import com.maderismawan.product.validation.annotation.ExitsProductId;

import io.micrometer.common.lang.Nullable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class CreateOutboundDTO {
    @Nullable
    private String RefId;

    @ExitsProductId
    private Long productId;

    @Min(1)
    private Integer qty;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RefTypeTransaction refType;
}
