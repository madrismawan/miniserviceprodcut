package com.maderismawan.product.managers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maderismawan.product.dto.inventoryTransaction.CreateOutboundDTO;
import com.maderismawan.product.dto.inventoryTransaction.ResponseCreateOutboundDTO;
import com.maderismawan.product.dto.stocks.StockReport;
import com.maderismawan.product.entities.Inventory;
import com.maderismawan.product.entities.InventoryTransaction;
import com.maderismawan.product.entities.Product;
import com.maderismawan.product.services.InventoryService;
import com.maderismawan.product.services.InventoryTransactionService;
import com.maderismawan.product.services.ProductService;
import com.maderismawan.product.services.ReportService;

@Service
public class InventoryTransactionManager {
    @Autowired
    private final ProductService productService;

    @Autowired
    private final InventoryService inventoryService;

    @Autowired
    private final InventoryTransactionService inventoryTransactionService;

    @Autowired
    private final ReportService reportService;

    public InventoryTransactionManager(ProductService productService, InventoryService inventoryService, InventoryTransactionService inventoryTransactionService, ReportService reportService){
        this.productService = productService;
        this.inventoryService = inventoryService;
        this.inventoryTransactionService = inventoryTransactionService;
        this.reportService = reportService;
    }


    @Transactional
    public ResponseCreateOutboundDTO createOutbound(CreateOutboundDTO input){
        Product product = productService.findById(input.getProductId());
        if(product == null){    
            return new ResponseCreateOutboundDTO("Product Not Found", HttpStatus.NOT_FOUND);
        }
        StockReport stockByProduct = reportService.checkStockByProduct(input.getProductId());
        if(stockByProduct.getTotal() < input.getQty()){
            return new ResponseCreateOutboundDTO("Stock not enough", HttpStatus.BAD_REQUEST);
        }

        List<Inventory> inventories = inventoryService.finAllByProduct(product);
        inventories.forEach(invetory -> {
            Integer trx = invetory.getStockAmount() - input.getQty();   
            if(trx >= 0){
                InventoryTransaction inventoryTransaction = new InventoryTransaction()
                    .setRefId(input.getRefId())
                    .setCurrAmount(trx)
                    .setInventory(invetory)
                    .setTrxAmount(input.getQty())
                    .setPrevAmount(invetory.getStockAmount())
                    .setRefType(input.getRefType());
                inventoryTransactionService.create(inventoryTransaction);
                inventoryService.updateStockAmount(invetory.getId(), input.getQty());
                return;
            }
            InventoryTransaction inventoryTransaction = new InventoryTransaction()
                    .setRefId(input.getRefId())
                    .setCurrAmount(0)
                    .setInventory(invetory)
                    .setTrxAmount(invetory.getStockAmount())
                    .setPrevAmount(invetory.getStockAmount())
                    .setRefType(input.getRefType());
            inventoryTransactionService.create(inventoryTransaction);
            input.setQty(input.getQty() - invetory.getStockAmount());
            inventoryService.updateStockAmount(invetory.getId(), invetory.getStockAmount());
        });

        return new ResponseCreateOutboundDTO("Success create outbound", HttpStatus.OK);
    }


}
