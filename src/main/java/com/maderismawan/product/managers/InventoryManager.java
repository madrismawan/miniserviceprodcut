package com.maderismawan.product.managers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maderismawan.product.dto.inventories.CreateBatchInventoryDTO;
import com.maderismawan.product.dto.inventories.CreateInventoryDTO;
import com.maderismawan.product.entities.Inventory;
import com.maderismawan.product.entities.InventoryTransaction;
import com.maderismawan.product.entities.Product;
import com.maderismawan.product.services.InventoryService;
import com.maderismawan.product.services.InventoryTransactionService;
import com.maderismawan.product.services.ProductService;

import jakarta.persistence.EntityNotFoundException;

import java.util.ArrayList;
import java.util.List;

@Service
public class InventoryManager {
    @Autowired
    private final ProductService productService;

    @Autowired
    private final InventoryService inventoryService;

    @Autowired
    private final InventoryTransactionService inventoryTransactionService;

    public InventoryManager(ProductService productService, InventoryService inventoryService, InventoryTransactionService inventoryTransactionService){
        this.productService = productService;
        this.inventoryService = inventoryService;
        this.inventoryTransactionService = inventoryTransactionService;
    }

    @Transactional
    public Inventory create(CreateInventoryDTO input){
        Inventory inventory = input.toInventory();
        Product product = productService.findById(input.getProductId());
        if(product == null){    
            throw new EntityNotFoundException();
        }
        inventory.setProduct(product);
        inventoryService.create(inventory);
        Integer initStock = inventory.getStockAmount();
        String refId =  String.valueOf(inventory.getId());
        InventoryTransaction inventoryTransaction = new InventoryTransaction()
            .setRefId(refId)
            .setCurrAmount(initStock)
            .setInventory(inventory)
            .setTrxAmount(initStock)
            .setPrevAmount(0)
            .setRefType(input.getRefType());

        inventoryTransactionService.create(inventoryTransaction);
        return inventory;
    }

    public List<Inventory> findAllByProduct(Long product_id){
        Product product = productService.findById(product_id);
         if(product == null){    
            List<Inventory> inventoryList = null;
            return inventoryList;
        }
        List<Inventory> inventory = inventoryService.finAllByProduct(product);
        return inventory;
    }

    @Transactional
    public List<Inventory> createBatch(CreateBatchInventoryDTO input){
        CreateInventoryDTO inventory = new CreateInventoryDTO();
        inventory.setRefId(input.getRefId());
        inventory.setLocation(input.getLocation());
        inventory.setRefType(input.getRefType());
        inventory.setExpiredDate(input.getExpiredDate());
        
        List<Inventory> inventoryList = new ArrayList<>();
        input.getDetails().stream().forEach(detail -> {
            inventory.setQty(detail.getQty());            
            inventory.setProductId(detail.getProductId());
            Inventory newInventory = create(inventory);

            inventoryList.add(newInventory);
        });

        return inventoryList;
    }


}
