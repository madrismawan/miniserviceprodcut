package com.maderismawan.product.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maderismawan.product.entities.InventoryTransaction;

@Repository
public interface InventoryTransactionRepo extends JpaRepository<InventoryTransaction, Long>  {}
