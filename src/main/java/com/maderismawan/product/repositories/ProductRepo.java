package com.maderismawan.product.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maderismawan.product.entities.Product;

@Repository
public interface ProductRepo extends JpaRepository<Product, Long> {
    
}
