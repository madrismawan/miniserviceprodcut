package com.maderismawan.product.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maderismawan.product.dto.stocks.StockReport;
import com.maderismawan.product.entities.Inventory;
import com.maderismawan.product.entities.Product;

import java.util.List;

@Repository
public interface InventoryRepo extends JpaRepository<Inventory, Long> {
    List<Inventory> findByProduct(Product product);


}
