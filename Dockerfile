ARG JAVA_VERSION=17

FROM openjdk:${JAVA_VERSION}-alpine

ARG JAR_FILE=target/*.jar

COPY ${JAR_FILE} app.jar

ENTRYPOINT ["java","-jar","/app.jar"]
